class RogissuetrackingController < ApplicationController

  def index
  	@rogissuetracking = Rogissuetracking.where(:issue_id => params[:issueid])
  end

  def save
    if params[:xobjid].to_i > 0
      @rogissuetracking = Rogissuetracking.find(params[:xobjid])
      @rogissuetracking.description = params[:description]
      @rogissuetracking.fecha = params[:fecha]
      @rogissuetracking.status = params[:status]
      @rogissuetracking.updated_on = Time.now
      @rogissuetracking.save
    else
    	@rogissuetracking = Rogissuetracking.new
    	@rogissuetracking.description = params[:description]
      @rogissuetracking.fecha = params[:fecha]
      @rogissuetracking.issue_id = params[:roggedissueid]
      @rogissuetracking.author_id = session[:user_id]
      @rogissuetracking.status = params[:status]
      @rogissuetracking.created_on = Time.now
      @rogissuetracking.updated_on = Time.now
      @rogissuetracking.save
    end
	  redirect_to :back
  end

  def delete
    @rogissuetracking = Rogissuetracking.find(params[:id])
    @rogissuetracking.delete
    redirect_to :back
  end
end
