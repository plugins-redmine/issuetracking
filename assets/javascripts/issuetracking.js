$(document).ready(function() {
	if($("body.controller-issues #content .contextual .icon.icon-time-add").length>0){
		xissueid=$('#issue-form').attr('action');
		xissueid=xissueid.replace('/issues/','');
		$("body.controller-issues #content .contextual .icon.icon-time-add").before('<a class="icon icon-add fancypopup-rogissuetracking dnone" href="/issuetracking/?issueid='+xissueid+'">Tracking</a>');
		var w = 912, h = 520;
		$(".fancypopup-rogissuetracking").click(function(){
			window.open($(this).attr('href')+'#rogged-issuetracking','new','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width='+w+',height='+h+', top=100, left=100');
			return false;
		});

		function change_tipoissue(){
			if($('#issue_tracker_id').length > 0){
				xissue_tracker=$('#issue_tracker_id :selected').text();
				if(xissue_tracker.toLowerCase().trim() == 'proyectos' || xissue_tracker.toLowerCase().trim() == 'proyecto'){
					$('.icon-add.fancypopup-rogissuetracking').removeClass('dnone');
				}else{
					$('.icon-add.fancypopup-rogissuetracking').addClass('dnone');
				}
			}
		}
		change_tipoissue();
	}
	if($("body.controller-rogissuetracking").length>0){
		$('.rogged-more').click(function(){
			if( $(this).parent().find('.dtext').hasClass('vmore') ){
				$(this).html('Minimizar detalle');
				$(this).parent().find('.dtext').removeClass('vmore').addClass('vmoreshow');
			}else{
				$(this).html('Ver detalle completo');
				$(this).parent().find('.dtext').addClass('vmore').removeClass('vmoreshow');;
			}
			return false;
		});
	}
});