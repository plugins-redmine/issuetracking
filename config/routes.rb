# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
get 'issuetracking', :to => 'rogissuetracking#index'
post 'post/issuetracking/save', :to => 'rogissuetracking#save'
post 'post/issuetracking/delete', :to => 'rogissuetracking#delete'