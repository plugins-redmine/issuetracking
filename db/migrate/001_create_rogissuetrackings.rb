class CreateRogissuetrackings < ActiveRecord::Migration
  def change
    create_table :rogissuetrackings do |t|
      t.text :description
      t.date :fecha
      t.integer :issue_id, :default => 0
      t.integer :author_id, :default => 0
      t.integer :status, :default => 0
      t.datetime :created_on
      t.datetime :updated_on
    end
  end
end
