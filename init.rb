require_dependency 'issuetracking_hook_listener'

Redmine::Plugin.register :issuetracking do
  name 'Redmine Issue tracking plugin'
  author 'Oscar Torres'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://orotorres.com'
  author_url 'http://orotorres.com'
  #creta model
  # v2 bundle exec ruby script/rails generate redmine_plugin_model issuetracking rogged_issue_tracking description:string fecha:datetime
  # v3 bundle exec rails generate redmine_plugin_model issuetracking rogged_issue_tracking description:string fecha:datetime

  #create controller
  # v2 bundle exec ruby script/rails generate redmine_plugin_controller issuetracking rogissuetracking index save
  # v3 bundle exec rails generate redmine_plugin_controller issuetracking rogissuetracking index save

  #register rows
  # v2 bundle exec ruby script/rails console
  # v3 bundle exec rails console
  # Rogissuetrackings.create(:description => 'Test1', :fecha => '2018-09-20 00:00:00')
  # rit=Rogissuetrackings.new(:description => "Test2", :fecha => "2018-09-20 00:00:00")

  # bundle exec rake redmine:plugins:migrate
end
