class IssuetrackingHookListener < Redmine::Hook::ViewListener
	def view_layouts_base_html_head(context)
		'<link rel="stylesheet" type="text/css" href="/plugin_assets/issuetracking/stylesheets/issuetracking.css?v=1.2" media="screen" /><script type="text/javascript" src="/plugin_assets/issuetracking/javascripts/issuetracking.js?v=1.2"></script>'
	end
end
